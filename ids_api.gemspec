#
# IDS API - API to communicate with IDS's health data hosting compliant
# infrastructure <https://ids-assistance.com/>.
# Copyright (C) 2017 Yealth <contact@yealth.io>
#
# This file is part of IDS API.
#
# IDS API is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IDS API is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with IDS API.  If not, see <http://www.gnu.org/licenses/>.
#
# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ids_api/version'

Gem::Specification.new do |spec|
  spec.name          = "ids_api"
  spec.version       = IdsApi::VERSION
  spec.authors       = ["Yealth\n\n"]
  spec.email         = ["support@yealth.io"]

  spec.summary       = %q{Communicate with health provider IDS infrastructure.}
  spec.description   = %q{Allows to use authenticating and logging processes on IDS infrastructure, based on SOAP services.}
  spec.homepage      = "https://framagit.org/yealth/ids_api"
  spec.license       = "LGPLv3"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'savon', '~> 2.11.0'

  spec.add_development_dependency "bundler", "~> 1.14"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "byebug", "~> 9.0.6"
  spec.add_development_dependency "webmock", "~> 3.0.1"
  spec.add_development_dependency "copyright-header", "~> 1.0.22"
end
