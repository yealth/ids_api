#
# IDS API - API to communicate with IDS's health data hosting compliant
# infrastructure <https://ids-assistance.com/>.
# Copyright (C) 2017 Yealth <contact@yealth.io>
#
# This file is part of IDS API.
#
# IDS API is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IDS API is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with IDS API.  If not, see <http://www.gnu.org/licenses/>.
#
require "spec_helper"

RSpec.describe IdsApi::Configuration do

  let(:configuration) { IdsApi::Configuration.instance }

  describe "logger" do
    it "default logger is set to STDOUT" do
      expect(configuration.app_logger).to be_a ::Logger
    end

    it "logger can be set in config file" do
      custom_logger = Logger.new(STDERR)
      IdsApi.configure do |config|
        config.app_logger = custom_logger
      end

      expect(configuration.app_logger).to eq custom_logger
    end
  end

end
