#
# IDS API - API to communicate with IDS's health data hosting compliant
# infrastructure <https://ids-assistance.com/>.
# Copyright (C) 2017 Yealth <contact@yealth.io>
#
# This file is part of IDS API.
#
# IDS API is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IDS API is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with IDS API.  If not, see <http://www.gnu.org/licenses/>.
#
require "spec_helper"

RSpec.describe IdsApi::Logger do

  let(:logger) { IdsApi::Logger.instance }

  it "default wsdl is set" do
    expect(logger.wsdl).to eq 'http://api.idshost.priv/log.wsdl'
  end

  describe "writing logs" do
    describe "happy path" do
      it "logs access for a specific patient" do
        # Given
        disable_app_logger
        access_log = IdsApi::LogAccess.new('app_name', 'page_name', 'requester', 'organization_unit', 'auth_cookie', 'unit', 'patient', 0, 'extra')
        stub_request(:get, 'http://api.idshost.priv/log.wsdl').to_return(status: 200, body: load_fixture_file('wsdl', 'log.wsdl'))
        stub_request(:post, 'http://api.idshost.priv/log.php').
          with(body: inline_xml(load_fixture_file('log_access', 'request.xml'))).
          to_return(status: 200, body: inline_xml(load_fixture_file('log_access', 'success.xml')), headers: {})
        # When
        response = IdsApi::Logger.write(access_log)
        # Then
        expect(response).to be_a Savon::Response
        expect(response).to be_success
      end
    end
  end

end
