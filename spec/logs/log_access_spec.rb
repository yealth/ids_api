#
# IDS API - API to communicate with IDS's health data hosting compliant
# infrastructure <https://ids-assistance.com/>.
# Copyright (C) 2017 Yealth <contact@yealth.io>
#
# This file is part of IDS API.
#
# IDS API is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IDS API is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with IDS API.  If not, see <http://www.gnu.org/licenses/>.
#
require "spec_helper"

RSpec.describe IdsApi::LogAccess do

  let(:log) { IdsApi::LogAccess.new('app_name', 'page_name', 'requester', 'organization_unit', 'auth_cookie', 'unit', 'patient', :consult, 'extra') }

  it "type is set to :log_access" do
    expect(log.type).to eq :log_access
  end

  it "needs 10 params" do
    expect(log.params['LogLine'].keys.length).to eq 10
  end

  it "LogDate must be nil" do
    expect(log.params['LogLine']['LogDate']).to be_nil
  end

end
