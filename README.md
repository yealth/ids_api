# IdsApi

TODO: description of the gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ids_api'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ids_api

## Usage

TODO: Write usage instructions here

## Configuration

In order to globally configure the gem within you application, use the following code in a `ruby` file (for example `config/initializers/ids_api.rb` for a Rails app).

```ruby
IdsApi.configure do |config|
  # Internal logger for the app
  # config.app_logger = ::Logger.new(STDOUT) # default: use ruby [logger](https://ruby-doc.org/stdlib/libdoc/logger/rdoc/Logger.html)
  # config.app_logger = ::Logger.new('/dev/null') # disable app logging
  # config.app_logger = Rails.logger # Use same logger as Ruby on Rails
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on ...

/ids_api.
