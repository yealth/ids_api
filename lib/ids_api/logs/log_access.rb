#
# IDS API - API to communicate with IDS's health data hosting compliant
# infrastructure <https://ids-assistance.com/>.
# Copyright (C) 2017 Yealth <contact@yealth.io>
#
# This file is part of IDS API.
#
# IDS API is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IDS API is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with IDS API.  If not, see <http://www.gnu.org/licenses/>.
#
require 'savon'

module IdsApi
  class LogAccess < Log

    attr_reader :params
    attr_reader :type

    def initialize(application, page_name, requester, organization_unit, auth_cookie, unit, patient, access_type, extra)
      @params =     {
        'LogLine' => {
          'LogDate' => nil,
          'Application' => application,
          'PageName' => page_name,
          'Requester' => requester,
          'OrganizationUnit' => organization_unit,
          'AuthCookie' => auth_cookie,
          'Unit' => unit,
          'Patient' => patient,
          'AccessType' => access_type,
          'Extra' => extra
        }
      }
      @type = :log_access
    end

  end
end
