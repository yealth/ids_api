#
# IDS API - API to communicate with IDS's health data hosting compliant
# infrastructure <https://ids-assistance.com/>.
# Copyright (C) 2017 Yealth <contact@yealth.io>
#
# This file is part of IDS API.
#
# IDS API is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IDS API is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with IDS API.  If not, see <http://www.gnu.org/licenses/>.
#
require "bundler/gem_tasks"
require "rspec/core/rake_task"

RSpec::Core::RakeTask.new(:spec)

task :default => :spec

task :headers do
  require 'rubygems'
  require 'copyright_header'

  args = {
    license: 'LGPL3',
    copyright_software: 'IDS API',
    copyright_software_description: "API to communicate with IDS's health data hosting compliant infrastructure <https://ids-assistance.com/>.",
    copyright_holders: ['Yealth <contact@yealth.io>'],
    copyright_years: ['2017'],
    add_path: 'lib:spec',
    remove_path: 'spec/fixtures',
    output_dir: '.'
  }

  command_line = CopyrightHeader::CommandLine.new( args )
  command_line.execute
end
